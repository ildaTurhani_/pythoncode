import random
choices = ["r", "p", "s"]

aiscore = 0
humanScore = 0

while (True):

    if (aiscore >=3):
        print("AI Won the game!!!!!!")
        print("Type 'new game' to play again. (Or type 'exit' to exit.)")
        newGame = input()
        if newGame == "new game":
            aiscore = 0
            humanScore = 0
            continue
        if newGame == "exit":
            break

    if (humanScore >=3):
        print("Human Won the Game!!!!")
        print("Type 'new game' to play again. (Or type 'exit' to exit.)")
        newGame = input()
        if newGame == "new game":
            aiscore = 0
            humanScore = 0
            continue
        if newGame == "exit":
            break

    ai_choice = random.choice(choices)
    human_choice = input('please make a selection  ')
    if (human_choice != "r") and (human_choice != "p") and (human_choice != "s"):
        print('Error, please choose between r, p or s')
    elif (human_choice == "r") and (ai_choice == "p") or (human_choice == "p") and (ai_choice == "s") or (human_choice == "s") and (ai_choice == "r"):
        aiscore += 1
        print( "AI wins.")
    elif human_choice == ai_choice:
        print ("Tie!" + " Please go again.")
    else:
        print("Human Wins.")
        humanScore +=  1

''' #Thank You for this one !
import random
choices=['r', 'p', 's']
triangle = {'r': 's', 's': 'p', 'p': 'r'}
aiscore=0
humanscore=0

while aiscore<3 and humanscore<3:
    ai_choice= random.choice(choices)
    human_choice= input(' Please make a selection')

    print(f'AI: {ai_choice}, Human: {human_choice}')


    if ai_choice == human_choice:
      print('Tie!')

    elif triangle[human_choice] == ai_choice:
        print('Human wins!')
        humanscore +=1
    else:
            print('Ai wins!')
            aiscore+=1
            print('Game over')
            if aiscore>humanscore:
                print('ai wins')
            else:
                print('player wins!')'''
